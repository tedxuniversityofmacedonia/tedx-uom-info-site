module.exports = {
  title: 'TEDxUniversityofMacedonia',
  hello: 'Learn everything about TEDxUniversityofMacedonia',
  welcome: 'Learn everything about TEDxUniversityofMacedonia.',
  aboutTED:
    'TED is a nonprofit organization devoted to Ideas Worth Spreading. Started as a four-day conference in California 30 years ago, TED has grown to support its mission with multiple initiatives.It began in 1984 in California as a conference focusing on Technology, Entertainment and Design. Today it covers all fields – from science and art to entrepreneurship and global issues.',
  aboutTEDx:
    'Ιn the spirit of ideas worth spreading, TEDx is a program of local, self-organized events that bring people together to share a TED-like experience.At a TEDx event, TED Talks video and live speakers combine to spark deep discussion and connection.These local, self-organized events are branded TEDx, where x = independently organized TED event. The TED Conference provides general guidance for the TEDx program, but individual TEDx events are self - organized.',
  aboutTEDxUoM:
    'TEDxUniversityofMacedonia is an institution of the University of Macedonia that aims to provide a step in ideas that are worth listening to and trigger change and action throughout the student community and more broadly in Thessaloniki’s young audience. It was founded in 2013 on “dare” . This was followed by the second TEDx conference on November 15th, 2014 on “ITHACAS”, inspired by Cavafy’s homonymous poem, inviting the new generation to ask about her own Ithaca. In turn, “Blind Spot” as the third event in 2015, has made us look at the issue from a different angle, the one that is usually hidden. TEDxUniversityofMacedonia 2016 returned for the fourth time with even greater ambitions, aiming at the gravity of an idea, in the “Gravity of Thoughts.” In 2017 TEDxUoM saw their own consciousness determine the great image of the world. That was the 5th event of TEDxUniversityofMacedonia. This year, in 2018, TEDxUoM will continue the game in a  bigger way than ever before. Just consider if you are ready to balance yourself with the people around you. It\'s up to you. Will you join?',
  TEDprompt: 'Learn about TED',
  TEDxprompt: 'Learn about TEDx',
  TEDxUoMprompt: 'Learn about TEDxUniversityofMacedonia',
  eventsIntro: 'TEDxUniversityofMacedonia has organized five events till today. Bellow you can find their themes and links to the respectives sites.',
}
