import React from 'react'
import { withIntl} from '../i18n'
import Layout from '../components/layout'
import './talks.css'

const VideoElement = ({ videoURL }) => {
  return (
    <div className="resp-container">
      <div className="resp-iframe">
        <iframe
          src={videoURL}
          frameBorder="0"
          allow="autoplay; encrypted-media"
          allowFullScreen
        />
      </div>
    </div>
  )
}

class Talks extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      activeYear: 2017,
      videoList: {
        2017: [
          'https://www.youtube.com/embed/Q0NlUQ-DXbg?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1',
          'https://www.youtube.com/embed/hDXwA0rlPVQ?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1',
          'https://www.youtube.com/embed/TNrPBlE_MEw?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1',
          'https://www.youtube.com/embed/g4Irau12lC0?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1',
          'https://www.youtube.com/embed/t8GUP87jwUY?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1',
          'https://www.youtube.com/embed/7si1CLY0-2w?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1',
          "https://www.youtube.com/embed/zQXqTD-t8TI?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1",
          "https://www.youtube.com/embed/5V8oxxYXzKg?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1",
          "https://www.youtube.com/embed/nJx_KHn-N4U?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1",
          "https://www.youtube.com/embed/QQvD8Qt0kP4?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1",
          "https://www.youtube.com/embed/BV_krecC8t8?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1",
          "https://www.youtube.com/embed/uZXSyr_bX-k?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1"
        ],
        2016: [
          'https://www.youtube.com/embed/pBGsk3b-e3Y?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/W_NR4-hcUNM?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/-CxeS0Qwbgk?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/gp8ubuLJANk?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/2RomtlAkHPw?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/nzb1jCZD9lA?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/nJEshLskpoQ?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/-Rc39t4IJks?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/8mDiC0e9Bqo?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/pgeSTfpp73E?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/SOgvN6m_WbM?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/DmawUhK4pSA?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/CoIGx8TR3Fg?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
          'https://www.youtube.com/embed/pGSQG0IyNTo?list=PLX0IQMHA-P-NjtJ_xrj0GmgPqvnbnyNnJ',
        ],
        2015: [
          'https://www.youtube.com/embed/kFlqkpS0SWE?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/NW84kycxMJY?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/XRXWDwwDLeM?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/4-BKlUwjK7w?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/aZMmoaZugJY?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/gX1mYpyz8BM?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/wxOIzVvxv8M?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/5-6qULM8kOI?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/oJi9Iy8c4dw?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/oqGiJaZgbVo?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/1ZDIqu-abg4?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
          'https://www.youtube.com/embed/2-7YyRDWtYo?list=PLX0IQMHA-P-MYSvSY7PyjrzN5-vOqgU71',
        ],
        2014: [
          'https://www.youtube.com/embed/eiOYAc051vY?list=PLX0IQMHA-P-NTPUOmDAIjvOhpV3JeX7Pg',
          'https://www.youtube.com/embed/BTlY-M7AU70?list=PLX0IQMHA-P-NTPUOmDAIjvOhpV3JeX7Pg',
          'https://www.youtube.com/embed/Z2rmetIzwUc?list=PLX0IQMHA-P-NTPUOmDAIjvOhpV3JeX7Pg',
          'https://www.youtube.com/embed/G_j-IUhy5c0?list=PLX0IQMHA-P-NTPUOmDAIjvOhpV3JeX7Pg',
          'https://www.youtube.com/embed/XjGGOxdc3NI?list=PLX0IQMHA-P-NTPUOmDAIjvOhpV3JeX7Pg',
          'https://www.youtube.com/embed/A-3Pyc4Qr2Q?list=PLX0IQMHA-P-NTPUOmDAIjvOhpV3JeX7Pg',
          'https://www.youtube.com/embed/NCJGQ_U0LNY?list=PLX0IQMHA-P-NTPUOmDAIjvOhpV3JeX7Pg',
          'https://www.youtube.com/embed/Q7QaFVuulaQ?list=PLX0IQMHA-P-NTPUOmDAIjvOhpV3JeX7Pg',
          'https://www.youtube.com/embed/riqafqkRNYs?list=PLX0IQMHA-P-NTPUOmDAIjvOhpV3JeX7Pg',
        ],
        2013: [
          'https://www.youtube.com/embed/FdFgEOnJ_PA?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/0d8q9OMzHIg?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/ve2-11Nepxc?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/M9tenX4FxhI?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/-9KcAOhLFhQ?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/HMafOi55E1w?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/UkcoVbHYsZk?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/mMW6a1EdG2s?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/NPTvBBWB9Is?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/aaUbG1xEQ0c?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/5jA5M3w7Q5U?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/h5CCPKu5C-U?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
          'https://www.youtube.com/embed/W-R4JVtMz5o?list=PLX0IQMHA-P-NFtxBq7Zt9dyn5fYUuQ-22',
        ],
      },
    }
  }

  handleYearClick = e => {
    this.setState({ activeYear: e.target.value })
  }

  render() {
    const { videoList, activeYear } = this.state
    let VideoGallery = <div />
    if (activeYear === -1) {
      VideoGallery = Object.keys(videoList).map(year => {
        return videoList[year].map((url, index) => (
          <VideoElement videoURL={url} key={index} />
        ))
      })
    } else {
      VideoGallery = videoList[activeYear].map((url, index) => (
        <VideoElement videoURL={url} key={index} />
      ))
    }

    return (
      <Layout>
        <div>
          <div className="year-selector-container">
            <ul>
              <li
                value="2017"
                onClick={this.handleYearClick}
                className={activeYear == 2017 ? 'active-year-button' : null}
              >
                2017
              </li>
              <li
                value="2016"
                onClick={this.handleYearClick}
                className={activeYear == '2016' ? 'active-year-button' : null}
              >
                2016
              </li>
              <li
                value="2015"
                onClick={this.handleYearClick}
                className={activeYear == '2015' ? 'active-year-button' : null}
              >
                2015
              </li>
              <li
                value="2014"
                onClick={this.handleYearClick}
                className={activeYear == '2014' ? 'active-year-button' : null}
              >
                2014
              </li>
              <li
                value="2013"
                onClick={this.handleYearClick}
                className={activeYear == '2013' ? 'active-year-button' : null}
              >
                2013
              </li>
              <li
                value="-1"
                onClick={this.handleYearClick}
                className={activeYear == '-1' ? 'active-year-button' : null}
              >
                All
              </li>
            </ul>
          </div>
          <div className="row">{VideoGallery}</div>
        </div>
      </Layout>
    )
  }
}

export default withIntl(Talks)
