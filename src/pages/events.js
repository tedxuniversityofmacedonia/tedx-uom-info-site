import React from 'react'
import { withIntl} from '../i18n'
import { FormattedMessage } from 'react-intl'
import Layout from '../components/layout'

const eventsInfo = [
  {
    year: 2017,
    theme: 'Consciousnet',
    url: 'https://www.tedxuniversityofmacedonia.com/',
  },
  {
    year: 2016,
    theme: 'Gravity of thoughts',
    url: 'https://2016.tedxuniversityofmacedonia.com/',
  },
  {
    year: 2015,
    theme: 'Blind Spot',
    url: 'https://2015.tedxuniversityofmacedonia.com/',
  },
  {
    year: 2014,
    theme: 'Ithacas',
    url: 'https://2014.tedxuniversityofmacedonia.com/',
  },
  {
    year: 2013,
    theme: 'Dare',
    url: 'https://2013.tedxuniversityofmacedonia.com/',
  },
]

const EventsList = () => {
  const temp = eventsInfo.map(({ year, theme, url }, index) => {
    return (
      <div key={index}>
        <h3>
          {theme}, <span>{year}</span>
        </h3>
        <p>
          <a href={url}>{url}</a>
        </p>
      </div>
    )
  })
  return <div>{temp}</div>
}

const Events = () => (
  <Layout>
    <div>
      <p>
        <FormattedMessage id="eventsIntro" />
      </p>
      <EventsList />
    </div>
  </Layout>
)

export default withIntl(Events)
