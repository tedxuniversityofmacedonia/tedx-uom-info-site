import React from 'react'
import Layout from '../components/layout'
import { FormattedMessage } from 'react-intl'
import { withIntl, Link } from '../i18n'

const IndexPage = () => (
  <Layout>
    <h3 className="about-intro-text">
      <FormattedMessage id="TEDprompt" />
    </h3>
    <p>
      <FormattedMessage id="aboutTED" />
    </p>
    <h3 className="about-intro-text">
      <FormattedMessage id="TEDxprompt" />
    </h3>
    <p>
      <FormattedMessage id="aboutTEDx" />
    </p>
    <h3 className="about-intro-text">
      <FormattedMessage id="TEDxUoMprompt" />
    </h3>
    <p>
      <FormattedMessage id="aboutTEDxUoM" />
    </p>
  </Layout>
)

export default withIntl(IndexPage)
